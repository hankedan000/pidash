/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidash.launcher;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daniel
 */
public class Config implements Serializable{
    private static String defaultSaveFile = "/home/daniel/pidash-launcher.cfg";
    private HashMap<String,Application> apps;
    
    public Config()
    {
        apps = new HashMap<>();
    }
    
    public Config(Config cfg)
    {
        apps = new HashMap<>(cfg.apps);
    }
    
    public void addApp(Application app)
    {
        apps.put(app.getName(), app);
    }
    
    public void removeApp(String name)
    {
        apps.remove(name);
    }
    
    public ArrayList<Application> getApps()
    {
        return new ArrayList<>(apps.values());
    }
    
    public boolean save()
    {
        boolean saved = true;
        
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(defaultSaveFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            
            // Serial config to file
            oos.writeObject(this);
            
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            System.err.printf("Caught exception while saving settings!\n%s\n",
                              ex.toString());
            saved = false;
        } catch (IOException ex) {
            Logger.getLogger(ConfigMenu.class.getName()).log(Level.SEVERE, null, ex);
            saved = false;
        }
        
        return saved;
    }
    
    public boolean restore()
    {
        boolean restored = true;
        
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(defaultSaveFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            
            // Serial config to file
            Config cfg = (Config)ois.readObject();
            this.apps = new HashMap<>(cfg.apps);
            
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            // Default config doesn't exist yet, lets try to save one
            if(!save())
            {
                System.err.println("Couldn't save default config!");
                restored = false;
            }
        } catch (IOException ex) {
            Logger.getLogger(ConfigMenu.class.getName()).log(Level.SEVERE, null, ex);
            restored = false;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return restored;
    }

}
