package pidash.launcher;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daniel
 */
public class Application implements Serializable{
    private String icon;
    private String name;
    private String exec;
    
    public Application()
    {
        icon = "";
        name = "";
        exec = "";
    }
    
    public void setIcon(String iconPath)
    {
        icon = iconPath;
    }
    
    public void setName(String appName)
    {
        name = appName;
    }
    
    public void setExec(String execCmd)
    {
        exec = execCmd;
    }
    
    public String getIcon()
    {
        return icon;
    }
    
    public String getName()
    {
        return name;
    }
    
    public boolean fromDesktopFile(File desktopFile)
    {
        boolean okay = true;
        
        try {
            Scanner in = new Scanner(desktopFile);
            
            while(in.hasNext())
            {
                if(in.findInLine("GenericName=") != null)
                {
                    // throw out
                } else if(in.findInLine("Name=") != null) {
                    this.name = in.nextLine();
                    System.out.println("Name = " + name);
                } else if(in.findInLine("Exec=") != null) {
                    this.exec = in.nextLine();
                    System.out.println("Exec = " + exec);
                } else if(in.findInLine("Icon=") != null) {
                    this.icon = in.nextLine();
                    System.out.println("Icon = " + icon);
                } else {
                    in.nextLine();
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
            okay = false;
        }
        
        return okay;
    }
    
    public boolean launch()
    {
        boolean okay = true;
        
        try {
            Runtime.getRuntime().exec(exec);
        } catch (IOException ex) {
            okay = false;
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE,
                                                              null,
                                                              ex);
        }
        
        return okay;
    }
    
    
}
