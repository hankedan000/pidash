/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidash.launcher;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import net.sf.image4j.codec.ico.ICODecoder;

/**
 *
 * @author daniel
 */
public class LauncherButton extends JButton implements ActionListener{
    private Application app;
    
    public LauncherButton(Application app)
    {
        String icon = app.getIcon();
        
        if(icon.contains(".ico"))
        {
            try {
                List<BufferedImage> ico = ICODecoder.read(new File(icon));
                if(ico.size()>0)
                {
                    setIcon(new ImageIcon(ico.get(0)));
                }
            } catch (IOException ex) {
                System.out.printf("%s icon doesn't exist!\n",icon);
            }
        } else {
            setIcon(new ImageIcon(icon));
        }
        
        this.app = app;
        this.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        app.launch();
    }
    
    
}
